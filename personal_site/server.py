# Importing and initalisation
from flask import Flask
from flask import send_file
app = Flask(__name__)

# Routing to main HTML page
@app.route('/')
def main():
    return send_file('index.html')

# Routing to main CSS page
@app.route('/css')
def css():
    return send_file('main.css')

# Routing to fonts
@app.route('/font/barcode')
def barfont():
    return send_file('fonts/LibreBarcode39Text-Regular.ttf')
@app.route('/font/vcr')
def vcrfont():
    return send_file('fonts/VCR_OSD_MONO_1.001.ttf')
@app.route('/font/alien')
def alienfont():
    return send_file('fonts/Alien-Encounters-Bold-Italic.ttf')
@app.route('/font/nipon')
def niponfont():
    return send_file('fonts/Senobi-Gothic-Bold.ttf')
