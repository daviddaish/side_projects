# Takes a CSV, finds the column with content formatted like URLS, and finds the
# geolocations of each URL's server, outputting a CSV, which is written into
# the directory the original CSV came from.

# Expects:
#   Arg 1: A CSV with at least two columns, each titled respectively Name and
#          Site. Site must be a URL.

import pandas as pd
import sys
from urlparse import urlparse
import requests
import time


# The prospect data to work with.
data_dir = sys.argv[1]


print "Importing {}...".format(data_dir)
full_prospects = pd.read_csv(data_dir)
prospects = full_prospects[["Name", "Site"]]
data_urls = []

for row in prospects.itertuples():
    raw_row_site = row[2]
    row_index = row[0]

    # Check if the prospect has a URL listed.
    if type(raw_row_site) is str:
        # Get the raw URL
        parsed_url = urlparse(raw_row_site)

        # If the parsed url has no netloc, use path, otherwise use url.
        if len(parsed_url[1]) == 0:
            true_url = parsed_url[2]
        else:
            true_url = parsed_url[1]

        # Append the true url and it's row's index.
        data_urls.append((row_index, true_url))


print "Searching for geo data on {} urls of {} prospects...".format(len(data_urls), prospects.shape[0])
print "This should take around {} seconds...".format(len(data_urls))

# Use the IP Location Finder API by KeyCDN to get the important information.
lookup_date = time.strftime("%d/%m/%Y")
lookup_results = []
for index, url in enumerate(data_urls):
    sys.stdout.write('\r' + "Looking up URL #{} - {}...".format(index + 1, url[1]) + ' ' * 25)
    sys.stdout.flush()

    payload = {"host": url[1]}
    lookup_request = requests.get("https://tools.keycdn.com/geo.json", params=payload)

    if "data" in lookup_request.json():
        lookup_results.append((url[0], lookup_request.json()["data"]["geo"]))
    else:
        print lookup_request.json()


    time.sleep(1)

print " "

# Format the results into a dictionary and index list, to prepare it to be
# turned into a dataframe.
formatted_lookup_results = []
index_lookup_results = []
for result in lookup_results:
    index_lookup_results.append(result[0])
    formatted_lookup_results.append({
        "Server Loc - Country": result[1]["country_name"],
        "Server Loc - City": result[1]["city"],
        "ISP": result[1]["isp"],
        "Lookup_date": lookup_date
    })

# Create a dataframe from the results.
results_dataframe = pd.DataFrame(data=formatted_lookup_results, index=index_lookup_results)

# Concat the original dataframe with the new info.
final_dataframe = pd.concat([full_prospects, results_dataframe], axis=1)

# Write the dataframe to a CSV.
old_loc_array = data_dir.split('/')
name = old_loc_array.pop()
new_name = name[0:-4] + " - GEO Lookup.csv"
loc_to_write_to_array = old_loc_array + [new_name]
final_file_loc = "/".join(loc_to_write_to_array)
final_dataframe.to_csv(path_or_buf=final_file_loc)

print "Written updated CSV to {}".format(final_file_loc)
print "Have a good day."
