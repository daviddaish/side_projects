# Personal site

This is largely just an excuse to do minor coding and use [Catalyst Cloud](https://www.catalyst.net.nz/catalyst-cloud).

## Running in development
```
cd <DIRECTORY CONTAINING personal_site>/personal_site
. server/py_serv/bin/activate
export FLASK_APP=server/__init__.py
export FLASK_DEBUG=1
flask run
```

## Running in production
These instructions build with these tutorials:
* [Installing the Openstack CLI](http://docs.catalystcloud.io/getting-started/cli.html#command-line-interface)
* [Hosting a static website in object storage](http://docs.catalystcloud.io/object-storage.html#static-websites-hosted-in-object-storage)

### Install the Openstack CLI
Using the above tutorials, install the Openstack CLI. There were a few hickups, and in case the documentation isn't updated by the time
I come back to this: Install `virtualenv` as well as `python python-pip python-virtualenv`. Also run `sudo apt-get install python-dev`
prior to pip installing `python-openstackclient`.

### Quick and dirty instructions
* Create openstack container
* Add static files to container
* make them public
* Point the html headers and CSS URLs to the new relevant public links
* Re-add the HTML and CSS files under the same name
* Redirect the domain name to the HTML file's public link, including https://
* Visit domain name
* Celebrate
